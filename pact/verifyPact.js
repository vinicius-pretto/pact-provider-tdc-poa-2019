const pact = require('@pact-foundation/pact-node');
const config = require('../config');
require('../index');

pact.verifyPacts(config.pact)
  .then(() => {
    console.log('Success!');
    process.exit(0);
  })
  .catch(error => {
    console.log('Failed!', error);
    process.exit(1);
  });