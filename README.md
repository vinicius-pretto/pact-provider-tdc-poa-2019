# Pact Provider for TDC POA 2019

## Requirements

* [Node.js](http://nodejs.org/)
* [Npm](https://www.npmjs.com/)
* [Pact Flow account](https://pactflow.io/sign-up/)

## Running tests

First you need to create an **.env** file with the following variables:

PACT_FLOW_TOKEN='xxxxxxxxxxxx-xxxxxxxxx'  
BITBUCKET_BUILD_NUMBER='1'

### Install dependencies

```
$ npm i
```

### Execute tests

```
$ npm t
```