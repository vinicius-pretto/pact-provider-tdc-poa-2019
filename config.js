require('dotenv').config();

const config = {
  port: process.env.PORT || 3000,
  pact: {
    providerBaseUrl: 'http://localhost:3000',
    providerStatesSetupUrl: 'http://localhost:3000/test/setup',
    pactUrls: ['https://raryson.pact.dius.com.au/pacts/provider/UsersService/consumer/WebApp/latest'],
    publishVerificationResult: true,
    providerVersion: process.env.BITBUCKET_BUILD_NUMBER,
    pactBrokerToken: process.env.PACT_FLOW_TOKEN
  }
};

module.exports = config;