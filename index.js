const app = require('./src/app');
const config = require('./config');

app.listen(config.port, () => {
  console.log(`Users service is listening at port ${config.port}`);
});